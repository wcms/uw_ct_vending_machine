<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <div class="vending_location_title"><?php print $title; ?></div>
<?php endif; ?>
<div class= "vending_machines">
<?php foreach ($rows as $id => $row): ?>
  <div<?php print $classes_array[$id] ? (' class="' . $classes_array[$id] . '"') : ''; ?>>
    <ul>
      <li><?php print $row; ?></li>
    </ul>
  </div>
<?php endforeach; ?>
</div>
