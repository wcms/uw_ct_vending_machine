<?php

/**
 * @file
 * uw_ct_vending_machine.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_vending_machine_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_vending_machine_info|node|uw_fs_vending_machine|form';
  $field_group->group_name = 'group_vending_machine_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_fs_vending_machine';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Vending Machine Information',
    'weight' => '1',
    'children' => array(
      0 => 'field_vending_machine_building',
      1 => 'field_vending_machine_product',
      2 => 'field_vending_machine_location',
      3 => 'field_vending_machine_map',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_vending_machine_info|node|uw_fs_vending_machine|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Vending Machine Information');

  return $field_groups;
}
