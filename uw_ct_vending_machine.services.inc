<?php

/**
 * @file
 * uw_ct_vending_machine.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function uw_ct_vending_machine_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'uwaterloo_services_vending';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'api/v1/vending';
  $endpoint->authentication = array(
    'services_api_key_auth' => array(
      'api_key' => '96ab9383e6ad48c23aa1504dc9cc5c52',
      'api_key_source' => 'request',
      'user' => 'WCMS web service user',
    ),
  );
  $endpoint->server_settings = array();
  $endpoint->resources = array(
    'vending_locations' => array(
      'operations' => array(
        'index' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['uwaterloo_services_vending'] = $endpoint;

  return $export;
}
