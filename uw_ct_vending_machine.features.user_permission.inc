<?php

/**
 * @file
 * uw_ct_vending_machine.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_vending_machine_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_fs_vending_machine content'.
  $permissions['create uw_fs_vending_machine content'] = array(
    'name' => 'create uw_fs_vending_machine content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_fs_vending_machine content'.
  $permissions['delete any uw_fs_vending_machine content'] = array(
    'name' => 'delete any uw_fs_vending_machine content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_fs_vending_machine content'.
  $permissions['delete own uw_fs_vending_machine content'] = array(
    'name' => 'delete own uw_fs_vending_machine content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_fs_vending_machine content'.
  $permissions['edit any uw_fs_vending_machine content'] = array(
    'name' => 'edit any uw_fs_vending_machine content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_fs_vending_machine content'.
  $permissions['edit own uw_fs_vending_machine content'] = array(
    'name' => 'edit own uw_fs_vending_machine content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view location directory'.
  $permissions['view location directory'] = array(
    'name' => 'view location directory',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'location',
  );

  return $permissions;
}
