/**
 * @file
 * Adds the ability to select a pre-existing location, making an AJAX call to populate the data from a central location.
 */

(function ($) {
  Drupal.behaviors.uw_ct_vending_machine = {
    attach: function (context, settings) {
     if ($("#autofill-location-data").length === 0) {
      $locations = $('<select>').addClass('form-select').attr('id', settings.uw_ct_vending_machine.selectId);

      // Create the list of locations.
      if (settings.uw_ct_vending_machine.data) {
        // Add first location placeholder.
        $locations.append($('<option>').text(settings.uw_ct_vending_machine.custom).attr('value', 'custom'));

        // Add location selections.
        $.each(settings.uw_ct_vending_machine.data, function (key, value) {
          $locations.append($('<option>').text(value.name).attr('data-name', value.name).attr('value', key));
        });

        // Add the list to the page.
        $locations.insertBefore('#' + settings.uw_ct_vending_machine.containerId, context)
        $locations.wrap($('<div>').addClass('form-item'));
        $locations.before($('<label>').text(settings.uw_ct_vending_machine.label).attr('for', settings.uw_ct_vending_machine.selectId).append(' <abbr title="This field is required." class="form-required">* <span>(required)</span></abbr>'));
        $locations.after($('<div>').text(settings.uw_ct_vending_machine.description).addClass('description'));

        // Set location data when the list changes.
        $locations.change(function () {
          if ($locations.val() != 'custom') {
            _populate_location_data(settings.uw_ct_vending_machine.data[$locations.val()])
          }
          else {
            _populate_location_data(
              {
                name: '',
                additional: '',
                street: '',
                city: '',
                province: '',
                postal_code: '',
                country: 'ca',
                latitude: '',
                longitude: ''
              }
            );
          }
        });
      }

      select_location = $('#edit-field-vending-machine-building-und-0-value').val();
      if (select_location) {
       select_location_val = $('option[data-name="' + select_location + '"]', $locations).val();
       if (select_location_val) {
         $locations.val(select_location_val);
       }
       else {
         $('#autofill-location-data').closest('div').find('.description').append(' Your previously chosen location, <em>' + select_location + '</em> no longer exists.  Please choose a new location.');
       }
      }

     }
    }
  };

  // TODO: use a variable setting from Drupal for the field name / language id selector.
  function _populate_location_data(data) {
    $('#edit-field-vending-machine-building-und-0-value').val(data.name);
    $('#edit-field-vending-machine-map-und-0-name').val(data.name);
    $('#edit-field-location-und-0-country').val(data.country);
    $('#edit-field-location-und-0-locpick-user-latitude').val(data.latitude);
    $('#edit-field-location-und-0-locpick-user-longitude').val(data.longitude);
    $('#gmap-auto1map-locpick_latitude0').val(data.latitude);
    $('#gmap-auto1map-locpick_longitude0').val(data.longitude);
  }
  $(function () {
    // Hiding the building and map field on the add vending machine form.
    $('#edit-field-vending-machine-building').hide();
    $('#edit-field-vending-machine-map-und-0').hide();
  });

}(jQuery));
