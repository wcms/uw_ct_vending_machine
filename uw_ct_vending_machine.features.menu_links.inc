<?php

/**
 * @file
 * uw_ct_vending_machine.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_vending_machine_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_vending-machine-floor:admin/structure/taxonomy/uw_vending_machine_floor.
  $menu_links['menu-site-manager-vocabularies_vending-machine-floor:admin/structure/taxonomy/uw_vending_machine_floor'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_vending_machine_floor',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Vending machine floor',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_vending-machine-floor:admin/structure/taxonomy/uw_vending_machine_floor',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_vending-machine-products:admin/structure/taxonomy/vending_machine_products.
  $menu_links['menu-site-manager-vocabularies_vending-machine-products:admin/structure/taxonomy/vending_machine_products'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/vending_machine_products',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Vending machine products',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_vending-machine-products:admin/structure/taxonomy/vending_machine_products',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Vending machine floor');
  t('Vending machine products');

  return $menu_links;
}
