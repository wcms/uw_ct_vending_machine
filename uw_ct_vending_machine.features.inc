<?php

/**
 * @file
 * uw_ct_vending_machine.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_vending_machine_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_vending_machine_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_vending_machine_node_info() {
  $items = array(
    'uw_fs_vending_machine' => array(
      'name' => t('Vending Machine'),
      'base' => 'node_content',
      'description' => t('Food Service\'s vending machines'),
      'has_title' => '1',
      'title_label' => t('Vending Machine Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
